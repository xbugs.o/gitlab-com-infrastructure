variable "environment" {
  default = "cny"
}

## Azure

variable "arm_subscription_id" {}
variable "arm_client_id" {}
variable "arm_client_secret" {}
variable "arm_tenant_id" {}

# We need these variables as part of the virtual machine creation.
# These will go away as soon as we switch to pre-baked server images.
# - Daniele

variable "first_user_username" {}
variable "first_user_password" {}

// These are the new variables to connect to the newly created instance, which
// replace the two above.
variable "ssh_user" {}

variable "ssh_private_key" {}
variable "ssh_public_key" {}

variable "location" {
  default = "East US 2"
}

provider "azurerm" {
  subscription_id = "${var.arm_subscription_id}"
  client_id       = "${var.arm_client_id}"
  client_secret   = "${var.arm_client_secret}"
  tenant_id       = "${var.arm_tenant_id}"
}

## Chef
variable "chef_version" {
  default = "12.19.36"
}

variable "chef_repo_dir" {}

## AWS
provider "aws" {
  region = "us-east-1"
}

variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

## State storage
terraform {
  backend "s3" {}
}

### Vnet

module "vnet" {
  source               = "vnet"
  location             = "${var.location}"
  virtual_network_cidr = "10.192.0.0/13"
}

### Subnets

module "subnet-external-lb" {
  source              = "subnets/external-lb"
  location            = "${var.location}"
  subnet_cidr         = "10.192.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-api" {
  source              = "subnets/api"
  location            = "${var.location}"
  subnet_cidr         = "10.196.2.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-git" {
  source              = "subnets/git"
  location            = "${var.location}"
  subnet_cidr         = "10.196.4.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-sidekiq" {
  source              = "subnets/sidekiq"
  location            = "${var.location}"
  subnet_cidr         = "10.196.6.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-web" {
  source              = "subnets/web"
  location            = "${var.location}"
  subnet_cidr         = "10.196.8.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

### Virtul Machines

module "virtual-machines-api" {
  address_prefix      = "${module.subnet-api.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"cny\", \"gitlab-cluster-base\": \"prd\"}"
  chef_version        = "${var.chef_version}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_B8ms"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-api.resource_group_name}"
  source              = "../../modules/virtual-machines/api"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-api.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-git" {
  address_prefix      = "${module.subnet-git.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"cny\", \"gitlab-cluster-base\": \"prd\"}"
  chef_version        = "${var.chef_version}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_F16s"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-git.resource_group_name}"
  source              = "../../modules/virtual-machines/git"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-git.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-web" {
  address_prefix      = "${module.subnet-web.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"cny\", \"gitlab-cluster-base\": \"prd\"}"
  chef_version        = "${var.chef_version}"
  count               = 1
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_F16s"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-web.resource_group_name}"
  source              = "../../modules/virtual-machines/web"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-web.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-external-lb" {
  source              = "virtual-machines/external-lb"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-external-lb.resource_group_name}"
  subnet_id           = "${module.subnet-external-lb.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"cny\", \"gitlab-cluster-base\": \"prd\", \"gitlab_cluster_lb\": \"_default\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}
