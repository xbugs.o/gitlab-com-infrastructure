###########################################################
# This is specific to the ops environment
# and defines the mapping from monitoring hosts to backend
# services

resource "google_compute_url_map" "monitoring-lb" {
  name            = "${format("%v-monitoring-lb", var.environment)}"
  default_service = "${module.prometheus.google_compute_backend_service_self_link}"

  host_rule {
    hosts        = ["prometheus.ops.gitlab.net"]
    path_matcher = "prometheus"
  }

  path_matcher {
    name            = "prometheus"
    default_service = "${module.prometheus.google_compute_backend_service_self_link}"

    path_rule {
      paths   = ["/*"]
      service = "${module.prometheus.google_compute_backend_service_self_link}"
    }
  }

  ###################################

  host_rule {
    hosts        = ["prometheus-app.ops.gitlab.net"]
    path_matcher = "prometheus-app"
  }
  path_matcher {
    name            = "prometheus-app"
    default_service = "${module.prometheus-app.google_compute_backend_service_self_link}"

    path_rule {
      paths   = ["/*"]
      service = "${module.prometheus-app.google_compute_backend_service_self_link}"
    }
  }

  ###################################

  host_rule {
    hosts        = ["alerts.ops.gitlab.net"]
    path_matcher = "alerts"
  }
  path_matcher {
    name            = "alerts"
    default_service = "${module.alerts.google_compute_backend_service_self_link}"

    path_rule {
      paths   = ["/*"]
      service = "${module.alerts.google_compute_backend_service_self_link}"
    }
  }

  ###################################

  host_rule {
    hosts        = ["thanos-query.ops.gitlab.net"]
    path_matcher = "thanos-query"
  }
  path_matcher {
    name            = "thanos-query"
    default_service = "${module.thanos-query.google_compute_backend_service_self_link}"

    path_rule {
      paths   = ["/*"]
      service = "${module.thanos-query.google_compute_backend_service_self_link}"
    }
  }
}
