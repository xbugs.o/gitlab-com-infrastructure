variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "gitlab_com_zone_id" {}

resource "azurerm_availability_set" "DeployProd" {
  name                         = "DeployProd"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.DeployProd.id}"
}

resource "azurerm_public_ip" "deploy" {
  name                         = "deploy-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "deploy"
}

resource "azurerm_network_interface" "deploy" {
  name                    = "deploy"
  internal_dns_name_label = "deploy"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "deploy-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.67.3.101"
    public_ip_address_id          = "${azurerm_public_ip.deploy.id}"
  }
}

resource "aws_route53_record" "deploy" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "deploy.gitlab.com"
  type    = "CNAME"
  ttl     = "300"
  records = ["${azurerm_public_ip.deploy.fqdn}."]
}

data "template_file" "bootstrap-deploy" {
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.deploy.ip_address}"
    hostname            = "deploy.gitlab.com"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "deploy" {
  name                          = "deploy.gitlab.com"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.DeployProd.id}"
  network_interface_ids         = ["${azurerm_network_interface.deploy.id}"]
  vm_size                       = "Standard_A4_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-deploy"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "deploy.gitlab.com"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.bootstrap-deploy.rendered}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.deploy.ip_address}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "30s"
    }
  }
}

resource "azurerm_public_ip" "import8-public-ip1" {
  name                         = "import8-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "import8"
}

resource "azurerm_network_interface" "import8" {
  name                    = "import8"
  internal_dns_name_label = "import8"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "import8-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.67.3.8"
    public_ip_address_id          = "${azurerm_public_ip.import8-public-ip1.id}"
  }
}

data "template_file" "bootstrap-import8" {
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.import8-public-ip1.ip_address}"
    hostname            = "import8.gitlab.com"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "import8" {
  name                          = "import8"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.DeployProd.id}"
  network_interface_ids         = ["${azurerm_network_interface.import8.id}"]
  vm_size                       = "Standard_D4"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-import8"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "import8"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.bootstrap-import8.rendered}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.import8-public-ip1.ip_address}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "30s"
    }
  }
}
