image: "registry.gitlab.com/gitlab-com/gl-infra/ci-images/gitlab-com-infrastructure:latest"

stages:
  - format
  - validate
  - planning
  - deployment

before_script:
  - for version in $(find . -name .terraform-version |xargs cat |sort -u); do /.tfenv/bin/tfenv install ${version}; done

tf_format:
  stage: format
  script: |
    #!/bin/sh
    set +o pipefail
    echo "running tf format"  # there's no output for empty builds otherwise
    fmt_diff=$(git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHA} | grep '\.tf$' | xargs -I{} terraform fmt -write=false {} | sed '/^\s*$/d')
    if test -n "$fmt_diff"; then
      echo "******* Terraform formatting error:"
      echo ""
      echo $fmt_diff
      exit 1
    fi

tf_validate:
  stage: validate
  # Validate every environment that contains an updated terraform config
  # (`terraform validate` does not do a full recurse)
  only:
    variables:
      - $SSH_PRIVATE_KEY
      - $SSH_KNOWN_HOSTS
  script: |
    #!/bin/sh
    set +o pipefail
    echo "running tf validate"  # there's no output for empty builds otherwise
    # SSH setup for module cloning
    eval $(ssh-agent -s)
    echo "$SSH_PRIVATE_KEY" | base64 -d | tr -d '\r' | ssh-add - > /dev/null
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    chmod 644 ~/.ssh/known_hosts

    cp private/env_vars/common.env.example private/env_vars/common.env
    dir="environments" # For the "bin/tf-set-env" script
    envs=$(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA | grep 'environments/.*\.tf$'|sed 's,\(.*/\)[^/]*.tf,\1,g'|sort -u)
    for env in $envs; do
      tf_opts="-check-variables=false"
      (
        dirs_to_check=$(find modules "$env" -name "*.tf" -type f -exec dirname {} \; | sort | uniq)

        environment=$(basename "$env") # Also used on the "bin/tf-set-env" script
        cp private/env_vars/environment.env.example private/env_vars/$environment.env

        source "bin/tf-set-env"

        /bin/sh -e -c 'for d in $1; do (cd $d && echo "Checking $d for $0" && terraform init -backend=false && terraform validate $2 && tflint --error-with-issues); done' "$env" "$dirs_to_check" "$tf_opts"
      )
    done

#gprd_tf_plan:
#  stage: planning
#  environment:
#    name: gprd
#  script:
#    - cd environments/gprd && terraform init -input=false -backend-config="bucket=${STATE_S3_BUCKET}" -backend-config="key=${STATE_S3_KEY}" -backend-config="region=${STATE_S3_REGION}" && terraform plan -input=false | landscape

# NOTE: last time this was enabled the following problems were encountered:
#  1. our bootstrap tied to chef-repo. co chef-repo on ci -- no bootstrap
#  1. TF_var for terraform user is not scoped to 'staging' environment ('*'),
#     but was not available. This may be GitLab bug -- needs more testing.
#staging_tf_apply:
#  stage: deployment
#  environment:
#    name: staging
#  script:
#    - cd environments/staging && terraform init -input=false -backend-config="bucket=${STATE_S3_BUCKET}" -backend-config="key=${STATE_S3_KEY}" -backend-config="region=${STATE_S3_REGION}" && terraform apply -input=false
#  only:
#    - master
