set statement_timeout = 0;

TRUNCATE TABLE web_hooks CASCADE;

UPDATE users
      SET authentication_token = null, rss_token = null,
      encrypted_otp_secret = null, encrypted_otp_secret_iv = null, encrypted_otp_secret_salt = null;

TRUNCATE TABLE personal_access_tokens;

TRUNCATE TABLE oauth_applications;

TRUNCATE TABLE oauth_access_tokens;

DELETE
      FROM ci_variables
      USING projects, users
      WHERE ci_variables.project_id = projects.id
        AND projects.creator_id = users.id
        AND users.email NOT LIKE '%@gitlab.com';

UPDATE project_import_data
      SET encrypted_credentials = null, encrypted_credentials_iv = null, encrypted_credentials_salt = null;

UPDATE users
  SET email = 'ops-contact+testbed-' || id || '@gitlab.com', name = 'username-removed-' || id,
      notification_email = 'ops-contact+notif-testbed-' || id || '@gitlab.com'
  WHERE email NOT LIKE '%@gitlab.com';

UPDATE remote_mirrors SET enabled = 'f' WHERE enabled = 't';
TRUNCATE TABLE ci_runners;
UPDATE notes
  SET note = '!!! CONFIDENTIAL NOTE - REDACTED !!!',
      note_html = '!!! CONFIDENTIAL NOTE - REDACTED !!!'
  WHERE EXISTS (
    SELECT true
    FROM issues
    WHERE issues.confidential = 't'
    AND notes.noteable_type = 'Issue'
    AND notes.noteable_id = issues.id);

UPDATE issues
  SET title = CONCAT('CONFIDENTIAL ISSUE ', id),
      title_html = CONCAT('CONFIDENTIAL ISSUE ', id),
      description = '!!! CONFIDENTIAL ISSUE - REDACTED !!!',
      description_html = '!!! CONFIDENTIAL ISSUE - REDACTED !!!'
  WHERE confidential = 't';

UPDATE services SET active = 'f' WHERE active = 't';
UPDATE application_settings SET signup_enabled = 'f' WHERE signup_enabled = 't';

UPDATE projects
  SET shared_runners_enabled = 'f'
  WHERE shared_runners_enabled = 't';

UPDATE ci_builds
  SET status = 'canceled'
  WHERE type = 'Ci::Build'
  AND status NOT IN ('canceled', 'failed', 'success');

UPDATE ci_pipeline_schedules
  SET active = 'f'
  WHERE active = 't';
