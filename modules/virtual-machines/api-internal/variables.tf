variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "count" {}
variable "environment" {}
variable "first_user_password" {}
variable "first_user_username" {}
variable "gitlab_com_zone_id" {}
variable "instance_type" {}
variable "location" {}
variable "resource_group_name" {}
variable "subnet_id" {}
variable "tier" {}
